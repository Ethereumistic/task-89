import EventEmitter from "eventemitter3";
import image from "../images/planet.svg";

export default class Application extends EventEmitter {
  static get events() {
    return {
      READY: "ready",
    };
  }

  constructor() {
    super();
    this.loading = document.createElement("progress");
    this.loading.classList.add("loading");
    document.body.appendChild(this.loading);
    this._load();
  }

  async _load(url = "https://swapi.boom.dev/api/planets") {
    this.loading.style.display = "block";
    const res = await fetch(url);
    const data = await res.json();
    this._create(data);
    if (data.next) {
      this._load(data.next);
    } else {
      this.loading.style.display = "none";
      this.emit(Application.events.READY);
    }
  }

  _create(data) {
    for (const planet of data.results) {
      const box = document.createElement("div");
      box.classList.add("box");
      box.innerHTML = this._render({
        name: planet.name,
        terrain: planet.terrain,
        population: planet.population,
      });
      document.body.querySelector(".main").appendChild(box);
    }
  }

  _render({ name, terrain, population }) {
    return `
    <article class="media">
      <div class="media-left">
        <figure class="image is-64x64">
          <img src="${image}" alt="planet">
        </figure>
      </div>
      <div class="media-content">
        <div class="content">
        <h4>${name}</h4>
          <p>
            <span class="tag">${terrain}</span> <span class="tag">${population}</span>
            <br>
          </p>
        </div>
      </div>
    </article>
        `;
  }
}